<?php

class Home extends Controller 
{
    public function index($name = '')
    {
        echo 'index@HomeController'.'<br>';
        $user = $this->model('User');
        $user->name = $name;

        // echo $user->name; // should be done in a view
        $this->view('home/index', ['name' => $user->name]);
    }

    public function dbtest()
    {
        echo '<br>'.'dbtest@HomeController'.'<br>';  
        $users = $this->model('User')->test();
        print_r(gettype($users));
        //update to pass to view 
        $this->view('home/dbtest', ['users' => $users]);
    }


}