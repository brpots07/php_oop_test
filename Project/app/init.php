<?php

// Database
require_once 'database/dbconnect.abstract.php';
require_once 'database/mysql_functions.php';

// Core
require_once 'Core/App.php';
require_once 'Core/Controller.php';