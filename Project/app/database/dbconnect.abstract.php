<?php


abstract class dbconnection
{
    /*
    |--------------------------------------------------------------------------
    | Abstract dbconnection
    |--------------------------------------------------------------------------
    |
    | Database connection abstracted so connection to databse cannot be accessed 
    | directly. 
    | Connection details were added in class instead of as environment 
    | variables as per requirements
    |
    */
    private $servername = "localhost";
    private $username = "homestead";
    private $password = "secret";
    private $dbname = "homestead";
    private $conn = null;

    public function __construct()
    {
        try {
            echo '<br>' . '__construct@dbconnection' . '<br>';
            // TODO: should we only establish a single connection?
            $this->conn = new PDO(
                "mysql:host=$this->servername;dbname=$this->dbname",
                $this->username,
                $this->password
            );
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            print_r($this->conn->getAttribute(PDO::ATTR_DRIVER_NAME));
            return $this->conn;
        } catch (PDOException $e) {
            echo '<br>' . $e->getMessage();
        }
    }

}
