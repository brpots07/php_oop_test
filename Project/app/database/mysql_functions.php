<?php

class MySql_functions extends dbconnection
// implements database_interface
{
    
    private $db;
    /**
     * Connect to database
     */
    public function __construct()
    {
        $this->db = parent::__construct();
    }

    /**
     * Create/insert to database
     */
    public function create($data)
    { }


    /**
     * Read/Select from database
     */
    public function read($data)
    { }

    /**
     * Update/Edit values in the database
     */
    public function update($data)
    { }

    /**
     * Remove/Delet values from the database
     */
    public function delete($data)
    { }


    /**
     * Test query db connection
     */
    public function query($query, $params = array())
    {
        echo '<br>' . 'query@MySql_functions' . '<br>';
        $statement = $this->db->prepare($query);
        $statement->execute($params);
        if (explode(' ', $query)[0] == 'SELECT') {
            $data = $statement->fetchAll();
            return $data;
        }
    }
}
