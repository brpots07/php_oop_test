<?php

class App
{
    protected $controller = 'Home';

    protected $method = 'index';

    protected $params = array();

    public function __construct()
    {
        /**
         * GET request should be in the following form:
         * URL/controller/method/parameters
         */
        $url = $this->parseUrl();
        echo '__construct@App:' . '<br>';
        print_r($url);


        // Check if controller exists
        if (file_exists('../app/Controllers/' . $url[0] . '.php'))
        {
            // echo 'file_exists '. 
            $this->controller = $url[0];
            unset($url[0]);
        }

        require_once '../app/Controllers/'. $this->controller. '.php';
        $this->controller = new $this->controller;
        // var_dump($this->controller);

        // Check if method exists
        if (isset($url[1]))
        {
            if (method_exists($this->controller, $url[1]))
            {
                $this->method = $url[1];
                unset($url[1]);
            }
        }

        $this->params = $url ? array_values($url) : array();

        call_user_func_array([$this->controller,$this->method], $this->params);

    }

    /**
     * parse URL so that routing can be established
     * URL/controller/method/parameters
     */
    public function parseUrl()
    {
        if (isset($_SERVER['REQUEST_URI'])) {   
            $url =  $_SERVER['REQUEST_URI'];
            return explode('/', filter_var(ltrim(rtrim($url, '/'),'/'), FILTER_SANITIZE_URL));
        }
    }

}