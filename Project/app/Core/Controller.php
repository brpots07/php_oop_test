<?php

class Controller
{ 
    /**
     * Create a object of model class
     */
    public function model($model)
    {
        // echo 'model@Controller'. '<br>';
        // echo $model;
        // TODO: check if this exists
        require_once '../app/Models/' . $model . '.php';
        return new $model;
    }

    /**
     * Pass data to a view
     */
    public function view($view, $data = array())
    {
        // echo '<br>' . 'view@Controller - ' . $data['name'] . '<br>';
        require_once '../app/views/'. $view . '.php';
    }
}
