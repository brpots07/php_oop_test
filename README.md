# homestead_no_laravel

A project for using Homestead per project with out laravel.
Homestead gives us a virtual environment to work with.

## Setup guides
- [mvc template](https://github.com/ahegazy/php-mvc-skeleton)
- [CodeSource](https://www.youtube.com/playlist?list=PLfdtiltiRHWGXVHXX09fxXDi-DqInchFD)

- [db setup](https://www.youtube.com/watch?v=5Ilp18Z-WDI)